--[[
-- *****************************************************************************
-- * CheckListConfig frame and child frame "OnEvent" handler.                  *
-- * Most common event here will be "ADDON_LOADED" as it is used to set up     *
-- * your global variables from previous game sessions.                        *
-- *****************************************************************************
--]]
function CheckListConfig_OnEvent (self, event, ...)
  if (event == "ADDON_LOADED") then
    if (self:GetName() == 'CheckListUsePerCharacterList') then
      self:SetChecked(CheckListPerCharacterDB.Config.UsePerCharacterList)
    end
  end
end

--[[
-- *****************************************************************************
-- * Update the characters configuration settings for "UsePerCharacterList"    *
-- * in the "CheckListPerCharacterDB.Config table.                             *
-- *****************************************************************************
--]]
function CheckListUsePerCharacterList_OnClick (self, button, down)
  local name = string.gsub(self:GetName(), "CheckList", "")
  if (self:GetChecked()) then
    CheckListPerCharacterDB.Config[name] = true
    PlaySound("igMainMenuOptionCheckBoxOn")
  else
    CheckListPerCharacterDB.Config[name] = false
    PlaySound("igMainMenuOptionCheckBoxOff")
  end
end

--[[
-- *****************************************************************************
-- * Calculate the y for our frames :SetPoint(x, y)                            *
-- *                                                                           *
-- * Param frameName                                                           *
-- *  - Frame object returned by CreateFrame()                                 *
-- *****************************************************************************
 ]]
function CheckListConfig_yOffset (frameName)
  local Point, RelativeTo, RelativePoint, XOffset, YOffset = frameName:GetPoint()
  local Width, Height = frameName:GetSize()
  local y = YOffset - Height - 5

  return y
end

--[[
-- *****************************************************************************
-- * CheckList Configuration Frame for Blizzards Interface Addons Menu         *
-- *****************************************************************************
--]]

--[[
-- Create our CheckListConfig frame and set the parent to UIParent. Setting the
-- parent to UIParent is important for creating a category in the Interface
-- Addons menu.
--]]
panel = CreateFrame("Frame", "CheckListConfig", UIParent)

-- Set the name of our category for the Interface Addons menu.
panel.name = "CheckList"

-- Set the text we want to display for the "Title" of our "CheckListConfig"
-- frame.
local ConfigTitleFrame = CreateFrame("Frame", "CheckListConfigTitle", CheckListConfig)
ConfigTitleFrame:SetPoint("TOPLEFT", 10, -10)

local FrameTitleText = ConfigTitleFrame:CreateFontString("CheckListConfigTitleFS", "BACKGROUND", "GameFontNormalLarge")
FrameTitleText:SetPoint("TOPLEFT", ConfigTitleFrame)
FrameTitleText:SetText("CheckList")
ConfigTitleFrame.text = FrameTitleText

-- We set the ConfigTitleFrame's width and height to that of the text width and
-- height that was generated for the "Title". This way the frame is always the
-- same size as our text.
ConfigTitleFrame:SetSize(FrameTitleText:GetWidth(), FrameTitleText:GetHeight())

-- Set the text we want to display for the "description" of our
-- "CheckListConfig" frame.
local ConfigDescFrame = CreateFrame("Frame", "CheckListConfigDesc", CheckListConfig)
local y = CheckListConfig_yOffset(ConfigTitleFrame)
ConfigDescFrame:SetPoint("TOPLEFT", 10, y)

local FrameDescText = ConfigDescFrame:CreateFontString("CheckListConfigDescFS", "BACKGROUND", "GameFontHighlightSmall")
FrameDescText:SetSize(600, 10)
FrameDescText:SetPoint("TOPLEFT", ConfigDescFrame)
FrameDescText:SetWordWrap(true)
FrameDescText:SetText("Thank you for using the checklist addon. Below you will find the configuration settings for this addon. - FatGuyLaughn")
FrameDescText:SetJustifyH("LEFT")
ConfigDescFrame.text = FrameDescText

ConfigDescFrame:SetSize(FrameDescText:GetWidth(), FrameDescText:GetHeight())

-- Setting the Texture
-- local tex = ConfigDescFrame:CreateTexture(nil, "BACKGROUND")
-- tex:SetAllPoints()
-- tex:SetTexture(1, 1, 1, 0.5)

--[[
-- Create the "UsePerCharacterList" option. This allows the user to user a
-- personal list for the character instead of the global list.
-- Create our CheckListUsePerCharacterList frame, set the parent to
-- CheckListConfig that we created above, and inherit from the
-- CheckListCheckBoxTemplate XML CheckButton template in CheckList.xml file.
--]]
local CB = CreateFrame("CheckButton", "CheckListUsePerCharacterList", CheckListConfig, "CheckListCheckBoxTemplate")
local y = CheckListConfig_yOffset(ConfigDescFrame)
CB:SetPoint("TOPLEFT", 10, y)
-- Register our "CheckButton" for the "ADDON_LOADED" event. This will inform
-- WoW to run the "OnEvent" script against our addon.
CB:RegisterEvent("ADDON_LOADED")
--[[
-- When the "OnEvent" script is ran for the addon it will execute the handler
-- "CheckListUsePerCharacterList_OnEvent" and pass the following parameters to
-- the handler (self, event, ...). self refers to the "CheckButton" object.
-- event refers to the event e.g.("ADDON_LOADED", "PLAYER_LOGIN",
-- "PLAYER_LOGOUT", ETC). ... refers to extra arguments that might be passed for
-- differnt events. For a list of events vist
-- http://wowprogramming.com/docs/events.
--]]
CB:SetScript("OnEvent", CheckListConfig_OnEvent)
-- We set the "OnClick" script after the "OnEvent" so the
-- CheckListPerCharacterDB global variable is loaded and will be altered
-- correctly by the "OnClick" handler.
CB:SetScript("OnClick", CheckListUsePerCharacterList_OnClick)

-- Set the text we want to display out to the right of our "CheckButton".
local PerCharCBText = CB:CreateFontString("CheckListUserPerCharacterListFS", "BACKGROUND", "GameFontHighlight")
PerCharCBText:SetPoint("LEFT", CB, "RIGHT", 5, 0)
PerCharCBText:SetText("Use character list instead of global list")
CB.text = PerCharCBText

--[[
-- *****************************************************************************
-- * Add our frame to the Blizzard Interface Addons Menu                       *
-- *****************************************************************************
--]]
InterfaceOptions_AddCategory(panel)