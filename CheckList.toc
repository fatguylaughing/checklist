## Interface: 60100
## Title: CheckList
## Notes: Create check lists to track quest or manual entries.
## Author: FatGuyLaughing
## Version: 6.1
## SavedVariables: CheckListDB
## SavedVariablesPerCharacter: CheckListPerCharacterDB

# Call the .xml file first so the frame templates are created before the .lua
# files try to use them.
CheckList.xml
CheckList.lua
CheckListConfiguration.lua