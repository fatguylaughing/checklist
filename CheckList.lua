-- *****************************************************************************
-- * Set up variables.                                                         *
-- *****************************************************************************

-- Set up global variables.
CheckListDB = {}
CheckListDB.Config= {}
CheckListDB.ItemNumber = 1
CheckListDB.Lists = {}
CheckListPerCharacterDB = {}
CheckListPerCharacterDB.Config = {}

-- Set up local data variables.
local CheckListRealm = GetRealmName()
local CheckListCharacterName = GetUnitName("player")

-- Set up local image variables.
local CheckListHeaderBKG = "Interface\\QUESTFRAME\\ObjectiveTracker.blp"
local CheckListHideButton = "Interface\\BUTTONS\\UI-Panel-QuestHideButton.blp"
local CheckListHideButtonHighlightTexture = "Interface\\BUTTONS\\UI-Panel-MinimizeButton-Highlight.blp"

--[[
-- *****************************************************************************
-- * Hide the Checklist frame.                                                 *
-- *****************************************************************************
 ]]
function CheckListHideButton_OnClick(self, button, down)
  -- Toggle the "CheckListWrapperCheckList" frame to the opposite visibility.
  local CLVisible = CheckListWrapperCheckList:IsVisible()
  if (CLVisible == true) then
    CheckListWrapperCheckList:Hide()
  else
    CheckListWrapperCheckList:Show()
  end
end

function CheckListTitle_Get()
  if (CheckListPerCharacterDB.Config.UsePerCharacterList == true) then
  else
    return "Global Checklist"
  end
end

function CheckListCreateListItem(Item)
  -- Create a wrapper frame that will hold the "CheckButton", "Text", and
  -- "Button" (Remove Button).
  local Wframe = CreateFrame("Frame", "CheckListWrapperListItem" .. CheckListDB.ItemNumber, CheckListWrapperCheckList)
  if (CheckListDB.ItemNumber == 1) then
    Wframe:SetPoint("TOPLEFT", CheckListWrapperCheckList, 15, -35)
  else
    local LastFrameName = "CheckListWrapperListItem" .. CheckListDB.ItemNumber - 1
    local y = CheckListConfig_yOffset(LastFrameName)
    Wframe:SetPoint("TOPLEFT", CheckListWrapperCheckList, 15, y)
  end

  Wframe:SetHeight(23)
  Wframe:SetWidth(275)

  local CB = CreateFrame("CheckButton", "CheckButton" .. CheckListDB.ItemNumber, Wframe, "CheckListCheckBoxTemplate")
  CB:SetPoint("LEFT", Wframe)

  local text = CB:CreateFontString("MyFontString" .. CheckListDB.ItemNumber, nil, "ObjectiveFont")
  text:SetPoint("LEFT", CB, "RIGHT", 5, 0)
  text:SetText(Item)
  CB.text = text
end

function CheckListAddButton_OnClick(self, button, down)
  -- Retrieve the "CheckListAddItemEB" EditBox text.
  local Item = CheckListAddItemEB:GetText()

  -- Clear the "CheckListAddItemEB" EditBox text.
  CheckListAddItemEB:SetText("")

  if (Item ~= "") then
    -- Make sure our realm is in the realm list.
    if (type(CheckListDB.Lists[CheckListRealm]) ~= "table") then
      CheckListDB.Lists[CheckListRealm] = {}
    end

    -- Make sure our character is in the realm list.
    if (type(CheckListDB.Lists[CheckListRealm][CheckListCharacterName]) ~= "table") then
      CheckListDB.Lists[CheckListRealm][CheckListCharacterName] = {}
    end

    -- Create a "CheckBox" frame with text and add it to our checklist.
    CheckListCreateListItem(Item)

    -- Update our item counter
    CheckListDB.ItemNumber = CheckListDB.ItemNumber + 1
  end
end

-- Create a Frame of type "Frame" with a unique name of "CheckList" attached
-- to the parent frame named "UIParent".
local CL = CreateFrame("Frame", "CheckList", UIParent)
CL:SetMovable(true)
CL:EnableMouse(true)
CL:RegisterForDrag("LeftButton")
CL:SetScript("OnDragStart", CL.StartMoving)
CL:SetScript("OnDragStop", CL.StopMovingOrSizing)

-- Hard setting the dimensions to pixels.
CL:SetWidth(275)
CL:SetHeight(512)

-- Setting the "anchor" point for the "CheckList" frame.
CL:SetPoint("CENTER", UIParent, "CENTER")

-- Setting the Texture
-- local tex = CL:CreateTexture(nil, "BACKGROUND")
-- tex:SetAllPoints()
--tex:SetTexture(1, 1, 1, 0.5)

local CLWrapper = CreateFrame("Frame", "CheckListWrapperCheckList", CheckList)
CLWrapper:SetPoint("TOPLEFT", CheckList)
CLWrapper:SetWidth(CLWrapper:GetParent():GetWidth())
CLWrapper:SetHeight(CLWrapper:GetParent():GetHeight())

local CLHEADER = CreateFrame("Frame", "CheckListHeader", CheckListWrapperCheckList)
CLHEADER:SetWidth(CLHEADER:GetParent():GetWidth())
CLHEADER:SetHeight(70)
CLHEADER:SetPoint("TOPLEFT", CheckListWrapperCheckList)
local tex = CLHEADER:CreateTexture(nil, "BACKGROUND")
tex:SetAllPoints()
tex:SetTexture(CheckListHeaderBKG)
tex:SetTexCoord(0, 0.55, 0.05, 0.35)

local CLTitle = CreateFrame("Frame", "CheckListTitle", CheckListWrapperCheckList)
CLTitle:SetPoint("TOPLEFT", CheckListWrapperCheckList)
CLTitle:SetWidth(CLTitle:GetParent():GetWidth())
CLTitle:SetHeight(20)
local CLTitleText = CLTitle:CreateFontString("CheckListTitleFS", nil, "GameFontNormalMed2")
CLTitleText:SetSize(275, 20)
CLTitleText:SetPoint("TOPLEFT", CLTitle, 35, -3)
CLTitleText:SetText(CheckListTitle_Get())
CLTitleText:SetJustifyH("LEFT")
CLTitle.text = CLTitleText

local CLHB = CreateFrame("Button", "CheckListHideButton", CheckList)
CLHB:SetWidth(16)
CLHB:SetHeight(16)
CLHB:SetPoint("TOPRIGHT", CheckList, "TOPRIGHT", -5, -5)
CLHB:SetNormalTexture(CheckListHideButton)
CLHB:GetNormalTexture():SetTexCoord(0, 0.5, 0.5, 1)
CLHB:SetHighlightTexture(CheckListHideButtonHighlightTexture)
CLHB:GetHighlightTexture():SetBlendMode("ADD")
CLHB:SetPushedTexture(CheckListHideButton)
CLHB:GetPushedTexture():SetTexCoord(0.5, 1, 0.5, 1)
CLHB:SetScript("OnClick", CheckListHideButton_OnClick)

-- Create an "EditBox" at the bottom of the "CheckList" frame for entering new
-- check list items.
local CLEB = CreateFrame("EditBox", "CheckListAddItemEB", CheckListWrapperCheckList, "InputBoxTemplate")
CLEB:SetPoint("BOTTOMLEFT", CheckListWrapperCheckList)
CLEB:SetWidth(215)
CLEB:SetHeight(20)
CLEB:SetAutoFocus(false)

-- Create our "Button" at the bottom of the "CheckList" frame for entering new
-- Check list items.
local CLAB = CreateFrame("Button", "CheckListAddButton", CheckListWrapperCheckList, "UIPanelButtonTemplate")
CLAB:SetWidth(50)
CLAB:SetHeight(20)
CLAB:SetPoint("BOTTOMRIGHT")
CLAB:SetText("Add")
CLAB:SetScript("OnClick", CheckListAddButton_OnClick)